
import { POSTS_READ_ALL, POSTS_CREATE, POSTS_REMOVE } from './posts.actions.js';

export function postsReducer(state = [], action) {
    switch (action.type) {
        case POSTS_READ_ALL:
            const { posts } = action;
            return posts;
        case POSTS_CREATE:
            const { post } = action;
            return [post].concat(state);
        case POSTS_REMOVE:
            const { id } = action;
            return state.filter(post => post.id !== id);
        default:
            return state;
    }
}