import React, {Component} from 'react';
import postService from '../services/post-service.js';

class PostListItem extends Component {
    constructor(props) {
        super(props);

        this.removeClickHandler = this.removeClickHandler.bind(this);
        this.titleClickHandler = this.titleClickHandler.bind(this);
    }

    titleClickHandler(evt) {
        evt.preventDefault();

        postService.readOne(this.props.post.id)
            .then(post => {
                alert(JSON.stringify(post, null, '\t'));
            })
            .catch(err => console.error(err));
    }

    removeClickHandler(evt) {
        evt.preventDefault();

        this.props.onRemove && this.props.onRemove(this.props.post.id);
    }

    render() {
        return (<tr>
            <td><a href="#" onClick={this.titleClickHandler}>{this.props.post.title}</a></td>
            <td>{this.props.post.category}</td>
            <td>{this.props.post.content}</td>
            <td>
                <a href="#" className="btn btn-danger" onClick={this.removeClickHandler}>
                    <span className="glyphicon glyphicon-remove" aria-hidden="true" ></span>
                </a>
            </td>
        </tr>)
    }
}

export default PostListItem;