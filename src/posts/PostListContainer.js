import { connect } from 'react-redux';
import { readAllPosts, createPost, removePost } from './posts.actions.js';
import PostList from './PostList.js';

const mapStateToProps = (state, ownProps) => {
    return {
        posts: state.posts
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onInit: () => {
            dispatch(readAllPosts());
        },
        onItemCreated: (title, categories, content) => {
            dispatch(createPost(title, categories, content));
        },
        onItemRemoved: (postId) => {
            dispatch(removePost(postId));
        }

    }
};

const PostListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(PostList);

export default PostListContainer