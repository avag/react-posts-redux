import { connect } from 'react-redux';
import { createPost } from './posts.actions.js';
import { showError } from '../common/components/status.actions.js';
import PostCreateForm from './PostCreateForm.js';

const mapStateToProps = (state, ownProps) => {
    return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onItemCreated: (title, categories, content) => {
            dispatch(createPost(title, categories, content));
        },
        onInvalidSubmit: (message) => {
            dispatch(showError(new Error(message)));
        }
    };
};

const PostCreateFormContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(PostCreateForm);

export default PostCreateFormContainer