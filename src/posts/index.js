import React from 'react';
import PostCreateFormContainer from './PostCreateFormContainer.js';
import PostListContainer from './PostListContainer.js';

function Posts () {
    return (
        <div className="row posts">
            <div className="col-sm-6">
                <PostCreateFormContainer />
            </div>
            <div className="col-sm-6">
                <PostListContainer />
            </div>
        </div>
    )
}

export default Posts;
