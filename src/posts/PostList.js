
import React, {Component} from 'react';
import PostListItem from './PostListItem.js';

class PostList extends Component {
    constructor(props) {
        super(props);

        this.itemRemoveHandler = this.itemRemoveHandler.bind(this);
    }

    componentDidMount() {
        this.props.onInit();
    }

    itemRemoveHandler (id) {
        this.props.onItemRemoved(id);
    }

    render() {
        return (
            <table>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Content</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {this.props.posts.map(post => <PostListItem key={post.id} post={post} onRemove={this.itemRemoveHandler}/>)}
                </tbody>
            </table>
        )
    }
}

export default PostList;