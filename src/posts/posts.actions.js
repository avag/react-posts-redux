
import postService from '../services/post-service.js';
import { showInfo, showError } from '../common/components/status.actions.js';
import thunkify from '../common/utils/thunkify.js';

// Types
export const POSTS_READ_ALL = 'POSTS_READ_ALL';
export const POSTS_CREATE = 'POSTS_CREATE';
export const POSTS_REMOVE = 'POSTS_REMOVE';
// \Types

function readAllPostsCallback(posts) {
    return {
        type: POSTS_READ_ALL,
        posts
    };
}

function createPostCallback(post) {
    return {
        type: POSTS_CREATE,
        post
    };
}

function removePostCallback(response, postId) {
    return {
        type: POSTS_REMOVE,
        id: postId
    };
}

// Actions
export const readAllPosts = thunkify(
    () => postService.readAll(),
    () => showInfo({ message: 'Reading all posts...' }),
    readAllPostsCallback,
    (err) => showError(new Error('Fetching all posts has been failed'))
);

export const createPost = thunkify(
    (title, categories, content) => postService.create(title, categories, content),
    () => showInfo({ message: 'Creating post...' }),
    createPostCallback,
    (err) => showError(new Error('Creating of post has been failed'))
);

export const removePost = thunkify(
    (postId) => postService.remove(postId),
    () => showInfo({ message: 'Removing post...' }),
    removePostCallback,
    (err) => showError(new Error('Removing of post has been failed'))
);
// \Actions
