import React, {Component} from 'react';

class PostCreateForm extends Component {
    constructor() {
        super();

        this.state = {
            title: '', category: '', content: ''
        };

        this.submitHandler = this.submitHandler.bind(this);
    }

    //Handlers
    submitHandler(evt) {
        evt.preventDefault();

        if (!this.state.title || !this.state.category || !this.state.content) {
            this.props.onInvalidSubmit && this.props.onInvalidSubmit('All fields are required. Please fill them before submition!');
            return;
        }

        this.props.onItemCreated && this.props.onItemCreated(this.state.title, this.state.category, this.state.content);

        this.setState({
            title: '', category: '', content: ''
        });
    }

    changeHandlerFactory(fieldName) {
        return (evt) => {
            this.setState({[fieldName]: evt.target.value})
        };
    }
    // |Handlers

    render() {
        return (<form className="form-horizontal" action="" method="POST" onSubmit={this.submitHandler}>
            <div className="form-group">
                <label htmlFor="titleTxt" className="col-sm-2 control-label">Title</label>
                <div className="col-sm-10">
                    <input type="text" name="title" className="form-control" id="titleTxt" value={this.state.title} onChange={this.changeHandlerFactory('title')} />
                </div>
            </div>
            <div className="form-group">
                <label htmlFor="categoryTxt" className="col-sm-2 control-label">Category</label>
                <div className="col-sm-10">
                    <input type="text" name="category" className="form-control" id="categoryTxt" value={this.state.category} onChange={this.changeHandlerFactory('category')}/>
                </div>
            </div>

            <div className="form-group">
                <label htmlFor="contentTxt" className="col-sm-2 control-label">Content</label>
                <div className="col-sm-10">
                    <textarea rows="5" name="content" className="form-control" id="contentTxt" value={this.state.content} onChange={this.changeHandlerFactory('content')}>
                    </textarea>
                </div>
            </div>

            <div className="form-group">
                <div className="col-sm-offset-2 col-sm-10">
                    <button type="submit" className="btn btn-default">Create</button>
                </div>
            </div>
        </form>)
    }

}

export default PostCreateForm;