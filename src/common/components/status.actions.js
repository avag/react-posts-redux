
// Types
export const STATUS_ERROR = 'STATUS_ERROR';
export const STATUS_INFO = 'STATUS_INFO';
export const STATUS_SUCCESS = 'STATUS_SUCCESS';
// \Types

// Actions
export function showError(error, delay = 5000) {
    return showStatus(STATUS_ERROR, error, delay);
}

export function showInfo(info, delay = null) {
    return showStatus(STATUS_INFO, info, delay);
}

export function showSuccess(info, delay = 5000) {
    return showStatus(STATUS_SUCCESS, info, delay);
}
// \Actions

let timeoutId = null;

function showStatus(type, info, delay) {
    return function (dispatch) {
        clearTimeout(timeoutId);

        dispatch({
            type: type,
            info
        });

        //Permanent message
        if (delay == null) {
            return;
        }

        timeoutId = setTimeout(() => {
            dispatch({
                type: type,
                info: null
            });
        }, delay)
    };
}
