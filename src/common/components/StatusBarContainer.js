import React from 'react';
import { connect } from 'react-redux';
import { STATUS_ERROR, STATUS_SUCCESS } from './status.actions.js';

function StatusBar (props) {
    const { status } = props;
    if (!status) {
        return null;
    }

    return <div className={`alert alert-${status.type === STATUS_ERROR ? 'danger'
                                         : status.type === STATUS_SUCCESS ? 'success'
                                         : 'info'}`}>{status.info.message}</div>;
}

const mapStateToProps = (state, ownProps) => {
    return {
        status: state.status
    };
};

const StatusBarContainer = connect(
    mapStateToProps
)(StatusBar);

export default StatusBarContainer