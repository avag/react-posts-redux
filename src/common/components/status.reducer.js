
import { STATUS_ERROR, STATUS_SUCCESS, STATUS_INFO } from './status.actions.js';

export function statusReducer(state = null, action) {
    switch (action.type) {
        case STATUS_ERROR:
        case STATUS_SUCCESS:
        case STATUS_INFO:
            const { info } = action;
            if (!info) { return null; }

            return {
                type: action.type,
                info
            };
        default:
            return null;
    }
}