
export default function (promiseWrapper, infoAction, successAction, errorAction) {
    return function (...params) {
        return function (dispatch) {
            infoAction && dispatch(infoAction());
            promiseWrapper(...params)
                .then(data => {
                    successAction && dispatch(successAction(data, ...params));
                })
                .catch(err => {
                    errorAction && dispatch(errorAction(err));
                });
        };
    };
}