import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Posts from './posts';
import StatusBarContainer from './common/components/StatusBarContainer.js';

//redux
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { postsReducer } from './posts/posts.reducer.js';
import { statusReducer } from './common/components/status.reducer.js';
// \redux

const store = createStore(
    combineReducers({ posts: postsReducer, status: statusReducer }),
    applyMiddleware(thunk)
);

class App extends Component {
  render() {
    return (
        <Provider store={store}>
          <div className="App">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <h1 className="App-title">BLOG POSTS CRUD</h1>
            </header>
            <br/>
            <div className="container">
                <StatusBarContainer/>
                <Posts />
            </div>
          </div>
        </Provider>
    );
  }
}

export default App;
